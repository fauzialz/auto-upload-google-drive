import sys

import toml
from pydrive.auth import GoogleAuth
from pydrive.auth import ServiceAccountCredentials
from pydrive.drive import GoogleDrive
import os
import traceback

config_file = 'config/conf.toml'
folder_id_path = ''
client_secrets = ''

def google_credentials():
    g_login = GoogleAuth()
    scope = ['https://www.googleapis.com/auth/drive']
    g_login.credentials = ServiceAccountCredentials.from_json_keyfile_name(client_secrets, scope)
    return GoogleDrive(g_login)

def get_folder_id(config_path):
    print('Checking Google Drive folder id...')
    try:
        if os.stat(config_path).st_size == 0:
            return None
        f = open(config_path, 'r')
        folder_id = f.read()
        f.close()
    except FileNotFoundError:
        return ''

    return folder_id

def clear_folder_id(file):
    try:
        f = open(file, 'w')
        f.write('')
        f.close()
    except FileNotFoundError:
        return


def delete_drive_folder(folder_id):
    try:
        drive = google_credentials()

        folder = drive.CreateFile({'id': folder_id})
        folder.Delete()

        print('Folder Deleted.\n')
    except:
        traceback.print_exc()

# MAIN
if __name__ == '__main__':
    print('\nScript Running ----------\n')

    try:
        with open(config_file) as cfg:
            config = toml.loads(cfg.read())
            folder_id_path = config['folder_id_path']
            client_secrets = config['client_secrets']
        f_id = get_folder_id(folder_id_path)
        if f_id is None:
            print('No folder to delete')
            sys.exit(0)
        delete_drive_folder(f_id)
        clear_folder_id(folder_id_path)
    except:
        traceback.print_exc()

    print('\nScript Exit! -----------\n')
