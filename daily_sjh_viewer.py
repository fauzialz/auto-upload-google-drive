import datetime
import os
import time
import traceback

import toml
from pydrive.auth import GoogleAuth
from pydrive.auth import ServiceAccountCredentials
from pydrive.drive import GoogleDrive
from pydrive.files import ApiRequestError
from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer

config_file = 'config/conf.toml'
folder_id_path = ''
directory_to_watch = ''
parent_folder_id = ''
client_secrets = ''

def google_credentials():
    g_login = GoogleAuth()
    scope = ['https://www.googleapis.com/auth/drive']
    g_login.credentials = ServiceAccountCredentials.from_json_keyfile_name(client_secrets, scope)
    return GoogleDrive(g_login)

def get_extension(path):
    try:
        ext = path.split('.', 1)[1]
    except IndexError:
        return ''
    return ext


# create new folder
def create_new_folder_id(config_path):
    print('Google Drive folder doesn\'t exist.\nCreating new folder...')
    
    drive = google_credentials()

    today = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d')
    folder_name = 'Daily SJH ({})'.format(today)
    folder = drive.CreateFile({
        'title': folder_name,
        'mimeType': 'application/vnd.google-apps.folder',
        'parents': [{
            'id': parent_folder_id
        }]
    })
    folder.Upload()

    f = open(config_path, 'w')
    f.write(folder['id'])
    f.close()

    return folder['id']


def get_folder_id(config_path):
    print('Checking Google Drive folder id...')
    try:
        if os.stat(config_path).st_size == 0:
            return None
        f = open(config_path, 'r')
        folder_id = f.read()
        f.close()
    except FileNotFoundError:
        return create_new_folder_id(folder_id_path)

    return folder_id


def upload_csv(folder_id, path):
    if get_extension(path) == 'csv':
        print('Uploading new csv...')

        time.sleep(10)

        with open(path, 'r') as f:
            file_name = os.path.basename(f.name)
            file_drive = drive.CreateFile({
                'title': file_name,
                'parents': [{
                    'id': folder_id
                }]
            })
            file_drive.SetContentString(f.read())
            file_drive.Upload()


class Watcher:

    def __init__(self):
        self.observer = Observer()

    def run(self):
        event_handler = Handler()
        self.observer.schedule(event_handler, directory_to_watch)
        self.observer.start()
        try:
            while True:
                time.sleep(5)
        except Exception:
            self.observer.stop()

        self.observer.join()

class Handler(FileSystemEventHandler):

    @staticmethod
    def on_any_event(event, **kwargs):
        if event.is_directory:
            return None

        elif event.event_type == 'created':
            # Take any action here when a file is first created.
            print("New file detected - {}.".format(event.src_path))
            folder_id = get_folder_id(folder_id_path)
            try:
                upload_csv(folder_id, event.src_path)
                print('{}\t-----\tUpload Success.'.format(event.src_path))
            except ApiRequestError:
                create_new_folder_id(folder_id_path)
                print('Retrying...')
                Handler.on_any_event(event, )


# MAIN
if __name__ == '__main__':
    print('\nScript SetUp ------------\n')
    try:
        with open(config_file) as cfg:
            config = toml.loads(cfg.read())
            folder_id_path = config['folder_id_path']
            directory_to_watch = config['directory_to_watch']
            parent_folder_id = config['parent_folder_id']
            client_secrets = config['client_secrets']

        drive = google_credentials()

        print('Script Running ----------\n')

        print("Watching local directory..\n")
        w = Watcher()
        w.run()
    except Exception as e:
        traceback.print_exc()

    print('\nScript Exit! -----------\n')
