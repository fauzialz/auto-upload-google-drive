import datetime
import time

today = datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S')

for i in range(5):
    name = 'newfile-{}_{}.csv'.format(i, today)
    with open('log/' + name, 'w') as f:
        f.write('index,\ttext\n')
        for x in range(1000):
            f.write('{},\tTest Daily SJH Viewer\n'.format(x))